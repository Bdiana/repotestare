package AngajatiApp.repository;

import AngajatiApp.model.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class EmployeeMockTest_WBT {
    Employee employee1, employee2, employee3, employee4, employee5, employee6, employee7, employee8;
    EmployeeRepositoryInterface repo;

    @Before
    public void setUp() throws Exception {
        repo = new EmployeeMock();
        System.out.println("Suntem in Before Test!!");
    }

    @After
    public void tearDown() throws Exception {
        employee1 = null;
        repo = null;
        System.out.println("Suntem in After Test!!!");
    }

    @Test
    public void modifyEmployee1() {
        List<Employee> employeeList = new ArrayList<>();
        this.repo.setEmployeeList(employeeList);
        this.repo.modifyEmployeeFunction(employee1, DidacticFunction.TEACHER);
        List<Employee> expectedEmployeeList = new ArrayList<>();
        assertEquals(expectedEmployeeList, employeeList);
    }

    @Test
    public void modifyEmployee2() {
        employee1 = new Employee(5, DidacticFunction.ASISTENT);
        List<Employee> employeeList = new ArrayList<>();
        this.repo.setEmployeeList(employeeList);
        this.repo.modifyEmployeeFunction(employee1, DidacticFunction.TEACHER);
        List<Employee> expectedEmployeeList = new ArrayList<>();
        assertEquals(expectedEmployeeList, employeeList);
    }

    @Test
    public void modifyEmployee3() {
        employee1 = new Employee(5, DidacticFunction.ASISTENT);
        employee2 = new Employee(6, DidacticFunction.LECTURER);
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(employee2);
        //expected
        employee3 = new Employee(6, DidacticFunction.LECTURER);
        List<Employee> expectedEmployeeList = new ArrayList<>();
        expectedEmployeeList.add(employee3);

        //modify
        this.repo.setEmployeeList(employeeList);
        this.repo.modifyEmployeeFunction(employee1, DidacticFunction.TEACHER);
        assertEquals(expectedEmployeeList, employeeList);
    }

    @Test
    public void modifyEmployee4() {
        employee1 = new Employee(5, DidacticFunction.ASISTENT);
        employee2 = new Employee(5, DidacticFunction.ASISTENT);
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(employee2);
        // expected
        employee3 = new Employee(5, DidacticFunction.TEACHER);
        List<Employee> expectedEmployeeList = new ArrayList<>();
        expectedEmployeeList.add(employee3);

        //modify
        this.repo.setEmployeeList(employeeList);
        this.repo.modifyEmployeeFunction(employee1, DidacticFunction.TEACHER);
        assertEquals(expectedEmployeeList, employeeList);
    }

    @Test
    public void modifyEmployee5() {
        employee1 = new Employee(5, DidacticFunction.ASISTENT);
        employee2 = new Employee(6, DidacticFunction.CONFERENTIAR);
        employee3 = new Employee(5, DidacticFunction.ASISTENT);
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(employee2);
        employeeList.add(employee3);

        //expected
        employee4 = new Employee(6, DidacticFunction.CONFERENTIAR);
        employee5 = new Employee(5, DidacticFunction.TEACHER);
        List<Employee> expectedEmployeeList = new ArrayList<>();
        expectedEmployeeList.add(employee4);
        expectedEmployeeList.add(employee5);
        //modify
        this.repo.setEmployeeList(employeeList);
        this.repo.modifyEmployeeFunction(employee1, DidacticFunction.TEACHER);
        assertEquals(expectedEmployeeList, employeeList);
    }

    @Test
    public void modifyEmployee6() {
        employee1 = new Employee(5, DidacticFunction.ASISTENT);
        employee2 = new Employee(6, DidacticFunction.ASISTENT);
        employee3 = new Employee(7, DidacticFunction.CONFERENTIAR);
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(employee1);
        employeeList.add(employee2);

        //expected
        employee4 = new Employee(6, DidacticFunction.ASISTENT);
        employee5 = new Employee(7, DidacticFunction.CONFERENTIAR);
        List<Employee> expectedEmployeeList = new ArrayList<>();
        expectedEmployeeList.add(employee4);
        expectedEmployeeList.add(employee5);

        //modify
        this.repo.setEmployeeList(employeeList);
        this.repo.modifyEmployeeFunction(employee1, DidacticFunction.CONFERENTIAR);
        assertEquals(employee1.getFunction(), DidacticFunction.CONFERENTIAR);

    }

    @Test
    public void modifyEmployee7() {
        employee1 = new Employee(6, DidacticFunction.ASISTENT);
        employee2 = new Employee(6, DidacticFunction.ASISTENT);
        employee3 = new Employee(5, DidacticFunction.LECTURER);
        employee4 = new Employee(6, DidacticFunction.CONFERENTIAR);

        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(employee2);
        employeeList.add(employee3);
        employeeList.add(employee4);

        //expected
        employee5 = new Employee(6, DidacticFunction.TEACHER);
        employee6 = new Employee(5, DidacticFunction.LECTURER);
        employee7 = new Employee(6, DidacticFunction.TEACHER);

        List<Employee> expectedEmployeeList = new ArrayList<>();
        expectedEmployeeList.add(employee5);
        expectedEmployeeList.add(employee6);
        expectedEmployeeList.add(employee7);

        this.repo.setEmployeeList(employeeList);
        this.repo.modifyEmployeeFunction(employee1, DidacticFunction.TEACHER);

        assertNotEquals(DidacticFunction.TEACHER, employee1);
    }
}
