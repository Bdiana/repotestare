package AngajatiApp.repository;

import AngajatiApp.model.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class EmployeeMockTest_BBT {
    Employee employee1, employee2, employee3, employee4, employee5, employee6, employee7, employee8;
    EmployeeRepositoryInterface repo;

    @Before
    public void setUp() throws Exception {
        repo = new EmployeeMock();
        System.out.println("Suntem in Before Test!!");
    }

    @After
    public void tearDown() throws Exception {
        employee1 = null;
        repo = null;
        System.out.println("Suntem in After Test!!!");
    }

    @Test
    public void addEmployee1() {
        employee1 = new Employee("Popescu", "Cristian", "1851021345131", DidacticFunction.ASISTENT, 1900d);
        assertTrue(repo.addEmployee(employee1));
    }

    @Test
    public void addEmployee2() {
        employee2 = new Employee("Gav", "Catalin", "1870623151354", DidacticFunction.TEACHER, 24999d);
        assertTrue(repo.addEmployee(employee2));
    }

    @Test
    public void addEmployee3() {
        employee3 = new Employee("Pop", "Florin", "1650306214023", DidacticFunction.CONFERENTIAR, 25000d);
        assertTrue(repo.addEmployee(employee3));
    }

    @Test
    public void addEmployee4() {
        employee4 = new Employee("Filip", "Ioan", "1632963148521", DidacticFunction.ASISTENT, 25001d);
        assertTrue(repo.addEmployee(employee4));
    }

    @Test
    public void addEmployee5() {
        employee5 = new Employee("Miclaus", "Paul", "1910324342959", DidacticFunction.CONFERENTIAR, 1901d);
        assertTrue(repo.addEmployee(employee5));
    }

    @Test
    public void addEmployee6() {
        employee6 = new Employee("Bulgarean", "Adi", "1910514364026", DidacticFunction.LECTURER, 6600d);
        assertTrue(repo.addEmployee(employee6));
    }

    @Test
    public void addEmployee7() {
        employee7 = new Employee("Fizesan", "Vasile", "1852610346257", DidacticFunction.TEACHER, 25001d);
        assertTrue(repo.addEmployee(employee7));

    }

    @Test
    public void addEmployee8() {
        employee8 = new Employee("Null", "Null", "1234567891011", DidacticFunction.LECTURER, 15000d);
        assertTrue(repo.addEmployee(employee8));
    }


}